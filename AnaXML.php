<?php 
class AnaXML
{
	public static function getEstacoesAna($url)
	{
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/xml')); 
		//curl_setopt($ch, CURLOPT_PROXY, $proxy);
		//curl_setopt($ch, CURLOPT_PROXYUSERPWD, $proxyauth);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_TIMEOUT, 15);
		curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5);
		$dataXml = curl_exec($ch);
		curl_close($ch);

		return $dataXml;
	}

	public static function getAjusteXML($dataXml)
	{
		$dataXml =  preg_replace("#\<".'xs:schema'."(.*)/".'xs:schema'.">#iUs", "",$dataXml);

		$dataXml = preg_replace('#<DataTable xmlns="http://MRCS/">#', "", $dataXml);
		$dataXml = preg_replace('#</DataTable>#', "", $dataXml);
		$dataXml = preg_replace('#<diffgr:diffgram xmlns:msdata="urn:schemas-microsoft-com:xml-msdata" xmlns:diffgr="urn:schemas-microsoft-com:xml-diffgram-v1">#', "", $dataXml);
		$dataXml = preg_replace('#</diffgr:diffgram>#', "", $dataXml);
		//$dataXml = preg_replace('#<DocumentElement xmlns="">#', "", $dataXml);
		//$dataXml = preg_replace('#</DocumentElement>#', "", $dataXml);

		$dataXml = preg_replace('/xmlns[^=]*="[^"]*"/i', '', $dataXml);

		$dataXml = preg_replace('/diffgr:id[^=]*="[^"]*"/i', '', $dataXml);
		$dataXml = preg_replace('/msdata:rowOrder[^=]*="[^"]*"/i', '', $dataXml);

		//print_r( preg_replace("#\<".'diffgr:diffgram'."(.*)/".'diffgr:diffgram'.">#iUs", "",$dataXml) );

		return $xml = (array)simplexml_load_string($dataXml);
	}

	public static function getDadosHidrometeorologicosByEstacao($arrayEstacao)
	{
		foreach ($arrayEstacao as $x)

		$hour = $x[0];

		for ($i=0; $i < count($x); $i++)
		{ 
			if ( strstr($x[$i]->DataHora, '07:00:00') )
					$hour= $x[$i];
		}

		return $hour;
	}

	public static function getPrecipitacao($arrayEstacao)
	{
		$soma = 0;
		foreach ($arrayEstacao as $x)
		{
			foreach ($x as $chuva)
				$soma += (floAT)$chuva->Chuva;
		}

		return $soma;
	}
	
}
?>