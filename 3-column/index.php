<?php 
require_once 'curl_ana.php';

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="initial-scale=1.0">
    <title>Map Viewer (3-column layout)</title>
    <!-- <link rel="stylesheet" href="../assets/css/ol.css" /> -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css" />
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" />
    <style type="text/css">
      body { overflow: hidden; }

      .navbar-offset { margin-top: 50px; }
      #map { position: absolute; top: 50px; bottom: 0px; left: 0px; right: 0px; }
      #map .ol-zoom { font-size: 1.2em; }

      .zoom-top-opened-sidebar { margin-top: 5px; }
      .zoom-top-collapsed { margin-top: 45px; }

      .mini-submenu{
        display:none;  
        background-color: rgba(255, 255, 255, 0.46);
        border: 1px solid rgba(0, 0, 0, 0.9);
        border-radius: 4px;
        padding: 9px;  
        /*position: relative;*/
        width: 42px;
        text-align: center;
      }

      .mini-submenu-left {
        position: absolute;
        top: 60px;
        left: .5em;
        z-index: 40;
      }
      .mini-submenu-right {
        position: absolute;
        top: 60px;
        right: .5em;
        z-index: 40;
      }

      #map { z-index: 35; }

      .sidebar { z-index: 45; }

      .main-row { position: relative; top: 0; }

      .mini-submenu:hover{
        cursor: pointer;
      }

      .slide-submenu{
        background: rgba(0, 0, 0, 0.45);
        display: inline-block;
        padding: 0 8px;
        border-radius: 4px;
        cursor: pointer;
      }

    </style>
    <style>
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #map {
        height: 100%;
      }
    </style>
  </head>
  <body>
    <!-- <script type="text/javascript" src="../assets/js/ol.js"></script> -->
    <script type="text/javascript" src="//code.jquery.com/jquery-2.1.1.min.js"></script>
    <script type="text/javascript" src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <script type="text/javascript">

      function applyMargins() {
        var leftToggler = $(".mini-submenu-left");
        var rightToggler = $(".mini-submenu-right");
        if (leftToggler.is(":visible")) {
          $("#map .ol-zoom")
            .css("margin-left", 0)
            .removeClass("zoom-top-opened-sidebar")
            .addClass("zoom-top-collapsed");
        } else {
          $("#map .ol-zoom")
            .css("margin-left", $(".sidebar-left").width())
            .removeClass("zoom-top-opened-sidebar")
            .removeClass("zoom-top-collapsed");
        }
        if (rightToggler.is(":visible")) {
          $("#map .ol-rotate")
            .css("margin-right", 0)
            .removeClass("zoom-top-opened-sidebar")
            .addClass("zoom-top-collapsed");
        } else {
          $("#map .ol-rotate")
            .css("margin-right", $(".sidebar-right").width())
            .removeClass("zoom-top-opened-sidebar")
            .removeClass("zoom-top-collapsed");
        }
      }

      function isConstrained() {
        return $("div.mid").width() == $(window).width();
      }

      function applyInitialUIState() {
        if (isConstrained()) {
          $(".sidebar-left .sidebar-body").fadeOut('slide');
          $(".sidebar-right .sidebar-body").fadeOut('slide');
          $('.mini-submenu-left').fadeIn();
          $('.mini-submenu-right').fadeIn();
        }
      }

      $(function(){
        $('.sidebar-left .slide-submenu').on('click',function() {
          var thisEl = $(this);
          thisEl.closest('.sidebar-body').fadeOut('slide',function(){
            $('.mini-submenu-left').fadeIn();
            applyMargins();
          });
        });

        $('.mini-submenu-left').on('click',function() {
          var thisEl = $(this);
          $('.sidebar-left .sidebar-body').toggle('slide');
          thisEl.hide();
          applyMargins();
        });

        $('.sidebar-right .slide-submenu').on('click',function() {
          var thisEl = $(this);
          thisEl.closest('.sidebar-body').fadeOut('slide',function(){
            $('.mini-submenu-right').fadeIn();
            applyMargins();
          });
        });

        $('.mini-submenu-right').on('click',function() {
          var thisEl = $(this);
          $('.sidebar-right .sidebar-body').toggle('slide');
          thisEl.hide();
          applyMargins();
        });

        $(window).on("resize", applyMargins);

        //var map = new ol.Map({
        //  target: "map",
          //layers: [
          //  new ol.layer.Tile({
          //    source: new ol.source.OSM()
          //  })
          //],
          //view: new ol.View({
            //center: [0, 0],
            //zoom: 2
          //})
        //});
        applyInitialUIState();
        applyMargins();
      });
    </script>
  </head>
  <body>
    <div class="container">
      <nav class="navbar navbar-fixed-top navbar-default" role="navigation">
        <div class="container-fluid">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">SISTEMA DE ALERTA HIDROLÓGICO (SAH)</a>
          </div>
          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                  <li class="divider"></li>
                  <li><a href="#">One more separated link</a></li>
                </ul>
              </li>
            </ul>
            <form class="navbar-form navbar-left" role="search">
              <div class="form-group">
                <input type="text" class="form-control" placeholder="Pesquisar">
              </div>
              <button type="submit" class="btn btn-default">Enviar</button>
            </form>
            <ul class="nav navbar-nav navbar-right">
              <li><a href="#">Link</a></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">Menu <b class="caret"></b></a>
                <ul class="dropdown-menu">
                  <li><a href="#">Action</a></li>
                  <li><a href="#">Another action</a></li>
                  <li><a href="#">Something else here</a></li>
                  <li class="divider"></li>
                  <li><a href="#">Separated link</a></li>
                </ul>
              </li>
            </ul>
            </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
        </div>
      </nav>
      <div class="navbar-offset"></div>
      <div id="map">
      </div>
      <div class="row main-row">
        <div class="col-sm-4 col-md-3 sidebar sidebar-left pull-left">
          <div class="panel-group sidebar-body" id="accordion-left">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" href="#layers">
                    <i class="fa fa-list-alt"></i>
                    Menu
                  </a>
                  <span class="pull-right slide-submenu">
                    <i class="fa fa-chevron-left"></i>
                  </span>
                </h4>
              </div>
              <div id="layers" class="panel-collapse collapse in">
                <div class="panel-body list-group">
                  <a target="_blank" href="http://mapas-hidro.ana.gov.br/Usuario/DadoPesquisar.aspx" class="list-group-item">
                    <i class="fa fa-globe"></i> Werbsite ANA
                  </a>
                  <a target="_blank" href="http://www.inmet.gov.br/portal/index.php?r=estacoes/estacoesAutomaticas" class="list-group-item">
                    <i class="fa fa-globe"></i> Werbsite INMET
                  </a>
                  <a href="#" class="list-group-item">
                    <i class="fa fa-globe"></i> Website SINTEGRA
                  </a>
                </div>
              </div>
            </div>
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" href="#properties">
                    <i class="fa fa-list-alt"></i>
                    Properties
                  </a>
                </h4>
              </div>
              <div id="properties" class="panel-collapse collapse in">
                <div class="panel-body">
                  <p>
                  Lorem ipsum dolor sit amet, vel an wisi propriae. Sea ut graece gloriatur. Per ei quando dicant vivendum. An insolens appellantur eos, doctus convenire vis et, at solet aeterno intellegebat qui.
                  </p>
                  <p>
                  Elitr minimum inciderint qui no. Ne mea quaerendum scriptorem consequuntur. Mel ea nobis discere dignissim, aperiam patrioque ei ius. Stet laboramus eos te, his recteque mnesarchum an, quo id adipisci salutatus. Quas solet inimicus eu per. Sonet conclusionemque id vis.
                  </p>
                  <p>
                  Eam vivendo repudiandae in, ei pri sint probatus. Pri et lorem praesent periculis, dicam singulis ut sed. Omnis patrioque sit ei, vis illud impetus molestiae id. Ex viderer assentior mel, inani liber officiis pro et. Qui ut perfecto repudiandae, per no hinc tation labores.
                  </p>
                  <p>
                  Pro cu scaevola antiopam, cum id inermis salutatus. No duo liber gloriatur. Duo id vitae decore, justo consequat vix et. Sea id tale quot vitae.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-4 col-md-6 mid"></div>
        <div class="col-sm-4 col-md-3 sidebar sidebar-right pull-right">
          <div class="panel-group sidebar-body" id="accordion-right">
            <div class="panel panel-default">
              <div class="panel-heading">
                <h4 class="panel-title">
                  <a data-toggle="collapse" href="#taskpane">
                    <i class="fa fa-tasks"></i>
                    Painel de Informações 
                  </a>
                  <span class="pull-right slide-submenu">
                    <i class="fa fa-chevron-right"></i>
                  </span>
                </h4>
              </div>
              <div id="taskpane" class="panel-collapse collapse in">
                <div class="panel-body">
                  <p>
                  Lorem ipsum dolor sit amet, vel an wisi propriae. Sea ut graece gloriatur. Per ei quando dicant vivendum. An insolens appellantur eos, doctus convenire vis et, at solet aeterno intellegebat qui.
                  </p>
                  <p>
                  Elitr minimum inciderint qui no. Ne mea quaerendum scriptorem consequuntur. Mel ea nobis discere dignissim, aperiam patrioque ei ius. Stet laboramus eos te, his recteque mnesarchum an, quo id adipisci salutatus. Quas solet inimicus eu per. Sonet conclusionemque id vis.
                  </p>
                  <p>
                  Eam vivendo repudiandae in, ei pri sint probatus. Pri et lorem praesent periculis, dicam singulis ut sed. Omnis patrioque sit ei, vis illud impetus molestiae id. Ex viderer assentior mel, inani liber officiis pro et. Qui ut perfecto repudiandae, per no hinc tation labores.
                  </p>
                  <p>
                  Pro cu scaevola antiopam, cum id inermis salutatus. No duo liber gloriatur. Duo id vitae decore, justo consequat vix et. Sea id tale quot vitae.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="mini-submenu mini-submenu-left pull-left">
        <i class="fa fa-list-alt"></i>
      </div>
      <div class="mini-submenu mini-submenu-right pull-right">
        <i class="fa fa-tasks"></i>
      </div>
    </div>
  <script>

  function initMap()
  {

 var locations = [
      ['<h3>Santarém</h3> <strong>Nível: </strong>'+'<?php echo $arrayEstacaoSanterem->Nivel; ?>'+'<br><strong>Chuva: </strong>'+'<?php echo $precipitacaoSantarem; ?>'+'<br><strong>Data/Hora: </strong>'+'<?php echo $arrayEstacaoSanterem->DataHora; ?>', -2.4515902, -54.7727582, 8],
      
      ['<h3>Rio Branco</h3> <strong>Nível: </strong>'+'<?php echo $arrayRioBranco->Nivel; ?>'+'<br><strong>Chuva: </strong>'+'<?php echo $precipitacaoRioBranco; ?>'+'<br><strong>Data/Hora: </strong>'+'<?php echo $arrayRioBranco->DataHora; ?>', -9.9863287, -67.8661162, 7],

      ['<h3>Porto Velho</h3> <strong>Nível: </strong>'+'<?php echo $arrayPortoVelho->Nivel; ?>'+'<br><strong>Chuva: </strong>'+'<?php echo $precipitacaoPortoVelho; ?>'+'<br><strong>Data/Hora: </strong>'+'<?php echo $arrayPortoVelho->DataHora; ?>', -8.7622488, -63.9006122, 6],

      ['<h3>Óbidos</h3> <strong>Nível: </strong>'+'<?php echo $arrayEstacaoObidos->Nivel; ?>'+'<br><strong>Chuva: </strong>'+'<?php echo $precipitacaoObidos; ?>'+'<br><strong>Data/Hora: </strong>'+'<?php echo $arrayEstacaoObidos->DataHora; ?>', -1.7602979, -55.5228043, 5],

      ['<h3>Grajaú</h3> <strong>Nível: </strong>'+'<?php echo $arrayEstacaoGrajau->Nivel; ?>'+'<br><strong>Chuva: </strong>'+'<?php echo $precipitacaoGrajau; ?>'+'<br><strong>Data/Hora: </strong>'+'<?php echo $arrayEstacaoGrajau->DataHora; ?>', -5.8203227, -46.1782518, 4],

      ['<h3>Boa Vista</h3> <strong>Nível: </strong>'+'<?php echo $arrayEstacaoBoaVista->Nivel; ?>'+'<br><strong>Chuva: </strong>'+'<?php echo $precipitacaoBoaVista; ?>'+'<br><strong>Data/Hora: </strong>'+'<?php echo $arrayEstacaoGrajau->DataHora; ?>', 2.8071985, -60.7315811, 3],

      ['<h3>Bacabal</h3> <strong>Nível: </strong>'+'<?php echo $arrayEstacaoBacabal->Nivel; ?>'+'<br><strong>Chuva: </strong>'+'<?php echo $precipitacaoBacabal; ?>'+'<br><strong>Data/Hora: </strong>'+'<?php echo $arrayEstacaoBacabal->DataHora; ?>', -4.2378545, -44.7812713, 2],

      ['<h3>Altamira</h3> <strong>Nível: </strong>'+'<?php echo $arrayEstacaoAltamira->Nivel; ?>'+'<br><strong>Chuva: </strong>'+'<?php echo $precipitacaoAltamira; ?>'+'<br><strong>Data/Hora: </strong>'+'<?php echo $arrayEstacaoAltamira->DataHora; ?>',-3.2081546, -52.2261614, 1]
    ];

    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 5,
      center: new google.maps.LatLng(-4.627999, -50.7023727),
      mapTypeId: google.maps.MapTypeId.ROADMAP
    });

    var infowindow = new google.maps.InfoWindow();

    var marker, i;

    for (i = 0; i < locations.length; i++) { 
      marker = new google.maps.Marker({
        position: new google.maps.LatLng(locations[i][1], locations[i][2]),
        map: map
      });

      google.maps.event.addListener(marker, 'click', (function(marker, i) {
        return function() {
          infowindow.setContent(locations[i][0]);
          infowindow.open(map, marker);
        }
      })(marker, i));
    }
  }
  </script>
  <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js">
    </script>
   <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyANqxhTLRsg6uZDNSZMcA5SUQ1XkP_YZDE&signed_in=true&callback=initMap"></script>
</body>
</html>