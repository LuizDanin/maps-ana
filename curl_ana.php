<?php
require_once 'AnaXML.php';
# An HTTP GET request example
$date = new DateTime();
$dataAtual = $date->format('d/m/Y');

if(date('w') == 1)
{
	$diaInicial = new DateTime();
	$diaInicial->modify('-3 day');
	$diaInicial = $diaInicial->format('d/m/Y');

	$diaFinal = new DateTime();
	$diaFinal->modify('-1 day');
	$diaFinal = $diaFinal->format('d/m/Y');
}
else
{
	$diaInicial = new DateTime();
	$diaInicial->modify('-1 day');
	$diaInicial = $diaInicial->format('d/m/Y');

	$diaFinal = new DateTime();
	$diaFinal->modify('-1 day');
	$diaFinal = $diaFinal->format('d/m/Y');
}

$estacoes = [ 1=>['altamira'=>18850000], 2=>['bacabal'=>33290000], 3=>['boa vista'=>14620000],
			 4=>['grajau'=>33321000] ,5=>['obidos'=>17050001], 6=>['porto velho'=>15400000], 7=>['rio branco'=>13600002], 8=>['santarem'=>17900000] ];

foreach ($estacoes as $e)
	$url[key($e)] = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.current($e).'&dataInicio='.$dataAtual.'&dataFim='.$dataAtual;
foreach ($estacoes as $ex)
	$urlx[key($ex)] = 'telemetriaws1.ana.gov.br/ServiceANA.asmx/DadosHidrometeorologicos?codEstacao='.current($ex).'&dataInicio='.$diaInicial.'&dataFim='.$diaFinal;

//altamira
$dataXmlAltamira = AnaXML::getEstacoesAna($url['altamira']);
$xmlAltamira = AnaXML::getAjusteXML($dataXmlAltamira);
$arrayEstacaoAltamira = AnaXML::getDadosHidrometeorologicosByEstacao($xmlAltamira);

#altamira dia anterior
$dataXmlAltamiraAnterior = AnaXML::getEstacoesAna($urlx['altamira']);
$xmlAltamiraAnterior = AnaXML::getAjusteXML($dataXmlAltamiraAnterior);
$precipitacaoAltamira = AnaXML::getPrecipitacao($xmlAltamiraAnterior);

//bacabal
$dataXmlBacabal = AnaXML::getEstacoesAna($url['bacabal']);
$xmlBacabal = AnaXML::getAjusteXML($dataXmlBacabal);
$arrayEstacaoBacabal = AnaXML::getDadosHidrometeorologicosByEstacao($xmlBacabal);

#bacabal dia anterior
$dataXmlBacabalAnterior = AnaXML::getEstacoesAna($urlx['bacabal']);
$xmlBacabalAnterior = AnaXML::getAjusteXML($dataXmlBacabalAnterior);
$precipitacaoBacabal = AnaXML::getPrecipitacao($xmlBacabalAnterior);

//boa vista
$dataXmlBoaVista = AnaXML::getEstacoesAna($url['boa vista']);
$xmlBoaVista = AnaXML::getAjusteXML($dataXmlBoaVista);
$arrayEstacaoBoaVista = AnaXML::getDadosHidrometeorologicosByEstacao($xmlBoaVista);

#boa vista dia anterior
$dataXmlBoaVistaAnterior = AnaXML::getEstacoesAna($urlx['boa vista']);
$xmlBoaVistaAnterior = AnaXML::getAjusteXML($dataXmlBoaVistaAnterior);
$precipitacaoBoaVista = AnaXML::getPrecipitacao($xmlBoaVistaAnterior);

//grajau
$dataXmlGrajau = AnaXML::getEstacoesAna($url['grajau']);
$xmlGrajau = AnaXML::getAjusteXML($dataXmlGrajau);
$arrayEstacaoGrajau = AnaXML::getDadosHidrometeorologicosByEstacao($xmlGrajau);

#grajau dia anterior
$dataXmlGrajauAnterior = AnaXML::getEstacoesAna($urlx['grajau']);
$xmlGrajauAnterior = AnaXML::getAjusteXML($dataXmlGrajauAnterior);
$precipitacaoGrajau = AnaXML::getPrecipitacao($xmlGrajauAnterior);


//obidos
$dataXmlObidos = AnaXML::getEstacoesAna($url['obidos']);
$xmlObidos = AnaXML::getAjusteXML($dataXmlObidos);
$arrayEstacaoObidos = AnaXML::getDadosHidrometeorologicosByEstacao($xmlObidos);

#obidos dia anterior
$dataXmlObidosAnterior = AnaXML::getEstacoesAna($urlx['obidos']);
$xmlObidosAnterior = AnaXML::getAjusteXML($dataXmlObidosAnterior);
$precipitacaoObidos = AnaXML::getPrecipitacao($xmlObidosAnterior);

//porto velho
$dataXmlPortoVelho = AnaXML::getEstacoesAna($url['porto velho']);
$xmlPortoVelho = AnaXML::getAjusteXML($dataXmlPortoVelho);
$arrayPortoVelho = AnaXML::getDadosHidrometeorologicosByEstacao($xmlPortoVelho);

#porto velho dia anterior
$dataXmlPortoVelhoAnterior = AnaXML::getEstacoesAna($urlx['porto velho']);
$xmlPortoVelhoAnterior = AnaXML::getAjusteXML($dataXmlPortoVelhoAnterior);
$precipitacaoPortoVelho = AnaXML::getPrecipitacao($xmlPortoVelhoAnterior);

//rio branco
$dataXmlRioBranco = AnaXML::getEstacoesAna($url['rio branco']);
$xmlRioBranco = AnaXML::getAjusteXML($dataXmlRioBranco);
$arrayRioBranco = AnaXML::getDadosHidrometeorologicosByEstacao($xmlRioBranco);

#rio branco dia anterior
$dataXmlRioBrancoAnterior = AnaXML::getEstacoesAna($urlx['rio branco']);
$xmlRioBrancoAnterior = AnaXML::getAjusteXML($dataXmlRioBrancoAnterior);
$precipitacaoRioBranco = AnaXML::getPrecipitacao($xmlRioBrancoAnterior);


//santarem
$dataXmlSantarem = AnaXML::getEstacoesAna($url['santarem']);
$xmlSantarem = AnaXML::getAjusteXML($dataXmlSantarem);
$arrayEstacaoSanterem = AnaXML::getDadosHidrometeorologicosByEstacao($xmlSantarem);

#santarem dia anterior
$dataXmlSantaremAnterior = AnaXML::getEstacoesAna($urlx['santarem']);
$xmlSantaremAnterior = AnaXML::getAjusteXML($dataXmlSantaremAnterior);
$precipitacaoSantarem = AnaXML::getPrecipitacao($xmlSantaremAnterior);
?>